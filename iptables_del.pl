#!/usr/bin/perl

sub reverse_ip($)
{
    my ($ip) = @_;
    my @nums = split(/\./, $ip);
    return "$nums[3].$nums[2].$nums[1].$nums[0]";
}

MAIN:
{
    if (! @ARGV) {
        print "Usage: $0 IP\n";
        exit(0);
    }

    my $ip = $ARGV[0];
    my $ip2 = reverse_ip($ip);

    `/sbin/iptables -D INPUT -s $ip -j DROP 2>/dev/null`;
    `/sbin/iptables -D INPUT -s $ip2 -j DROP 2>/dev/null`;
}
