#!/usr/bin/perl

sub send_sms($$$)
{
    my ($FROM, $TO, $MSG) = @_;

    use IO::Socket;
    my $sock = new IO::Socket::INET(PeerAddr=>"mail.mobigen.com", PeerPort=>"25");
    my $welcome = <$sock>; # $welcome;
    print $sock "SEND-SMS $FROM $TO $MSG\r\n";
    close($sock);
}

MAIN:
{
    while (my $line = <STDIN>) {
        $line =~ s/\r?\n//g;
        my @row = split(/ /, $line);
        if ($row[0] eq '+OK' && $row[1] eq 'FAIL2BAN') {
            send_sms("01041119360", "01041119360", "server[vultr] fail2ban attacker IP=$row[2]");
        }
    }
}
